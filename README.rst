====================
 Reise Beschreibung
====================

This is `LaTeX`_ document with just a first page from german text of a 18.
century travel diary written by `Christoph Wenzel von Nostitz`_, as published
in book `Deník z cesty do Nizozemí v roce 1705`_. The document is typeseted in
`fraktur`_ using `blacklettert1`_ package (T1-alike encoded variants of
`Yannis Haralammbous's old German fonts`_).

Why? Well, just for fun :) It definitelly doesn't make any sense in any other
way.


.. image:: tagebuch.small.png
    :align: center


.. _`LaTeX`: https://en.wikipedia.org/wiki/LaTeX
.. _`fraktur`: https://en.wikipedia.org/wiki/Fraktur
.. _`blacklettert1`: http://ctan.org/pkg/blacklettert1
.. _`Yannis Haralammbous's old German fonts`: https://www.tug.org/TUGboat/tb12-1/tb31hara.pdf
.. _`Deník z cesty do Nizozemí v roce 1705`: https://www.worldcat.org/title/denik-z-cesty-do-nizozemi-v-roce-1705/oclc/56333560
.. _`Christoph Wenzel von Nostitz`: https://cs.wikipedia.org/wiki/Kry%C5%A1tof_V%C3%A1clav_z_Nostic
