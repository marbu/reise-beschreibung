.PHONY: all clean

all: tagebuch.pdf

%.pdf: %.tex
	pdflatex $? -o $@

%.dvi: %.tex
	latex $? -o $@

%.svg: %.dvi
	dvisvgm -n $? -o $@

clean:
	rm -f *.pdf *.dvi *.svg *.aux *.log
